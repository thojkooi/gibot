# GiBot - Github bot

[![build status](https://gitlab.com/thojkooi/gibot/badges/master/build.svg)](https://gitlab.com/thojkooi/gibot/commits/master)

Gibot is a Github issue helper bot and is reponsible for cleaning up old and abondened issues and replying to tickets that have been forgotten.

-----------

## Requirements

You can use gibot in two ways:

- 1) Local install.
- 2) Docker

For the local install, you require `nodeJS`. We test using the latest nodeJS (8.5.0) version. Earlier nodeJs versions may work, but are not tested or supported.

### Development

Install the dependencies using:

```bash
npm install
```
You can manually run it using `node app.js`.

### Environment variables

You will have to provide the following environment variables before being able to run the application:

| Variable | Description | Default |
|----------|-------------|---------|
| `GITHUB_TOKEN` | An Oauth token. Necessary for replying to issues and closing abondened issues. | - |
| `MAX_DAYS_NO_UPDATE` | The max amount of days a ticket can stay without updates before the bot will take action. | `14` |
| `USER_AGENT` | User agent name. | `Gibot Github bot` |
| `GITHUB_USER` | Github user or organisation | - |
| `GITHUB_REPO` | Github repository name.  | - |
| `CLOSING_LABELS` | A comma seperated list of labels for which the bot will close tickets. | `need more info,invalid` |
| `PRODUCTION` - boolean | true will have the bot actually close and comment on the tickets. | `true` |

## Usage

```
docker run -d -e PRODUCTION=true -e GITHUB_TOKEN=TOKEN registry.gitlab.com/thojkooi/gibot:latest
```

Or configure using docker-compose (above can easily be ported to a compose file).
