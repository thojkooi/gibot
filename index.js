'use strict';

const log4js = require('log4js');
const logger = log4js.getLogger('gibot');

const CronJob = require('cron').CronJob;
const GitHubApi = require('github');
const IssueChecker = require('./lib/issuesChecker.js');

const USER_AGENT = process.env.USER_AGENT || 'GiBot GitHub issue bot';
const GITHUB_USER = process.env.GITHUB_USER;
const GITHUB_REPO = process.env.GITHUB_REPO;
const GITHUB_TOKEN = process.env.GITHUB_TOKEN;
const PRODUCTION = process.env.PRODUCTION || false;

if (!GITHUB_TOKEN) {
    logger.error('No github token provided. Please set an github token as environment variable (-e GITHUB_TOKEN=TOKEN)');
    if (PRODUCTION) { // exit if we are using production configuration
        return;
    }
}

// set up our issue checking bot
const issueChecker = new IssueChecker({
    githubApi: new GitHubApi({
        protocol: 'https',
        host: 'api.github.com',
        headers: {
            'user-agent': USER_AGENT
        },
        followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
        timeout: 5000
    }),
    githubUser: GITHUB_USER,
    githubRepo: GITHUB_REPO,
    githubToken: GITHUB_TOKEN,
    production: PRODUCTION
});

function handleRepo() {
    issueChecker.fetchRepoConfigurationFile(function (err, configuration) {
        if (configuration && configuration.stages) {
            let seconds = 0;
            Object.keys(configuration.stages).forEach(function (stageName) {
                setTimeout(function () {
                    const stage = configuration.stages[stageName];
                    if (stage.labels && stage.labels.length > 0) {
                        issueChecker.checkRepositoryIssues(stage);
                    }
                }, seconds * 1000);
                seconds = seconds + 10;
            });
        }
    });
}

handleRepo();

const job = new CronJob('0 0 */1 * * *', function () {
    try {
        handleRepo();
    } catch (err) {
        logger.error(err);
    }
}, null, true);
