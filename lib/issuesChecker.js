'use strict';

const log4js = require('log4js');
const YAML = require('yamljs');
const logger = log4js.getLogger('gibot');

module.exports = function(config) {
    const github = config.githubApi;
    const MAX_DAYS_NO_UPDATE = config.maxDaysNoUpdate || 7;
    const GITHUB_USER = config.githubUser;
    const GITHUB_REPO = config.githubRepo;
    const GITHUB_TOKEN = config.githubToken;
    const PRODUCTION = config.production || false;

    function fetchRepoConfigurationFile(cb) {
        authenticate();
        github.repos.getContent({
            user: GITHUB_USER,
            repo: GITHUB_REPO,
            path: '.gibot.yml'
        }, function(err, result) {
            if (err) {
                if (err.code && err.code === 404) {
                    logger.info('Repository does not have a .gibot.yml configuration file. Using default configuration');
                    const configFile = YAML.load('.gibot.yml');
                    return cb(null, configFile);
                }
                logger.error(err);
                return cb(new Error('Failed to find configuration file in repository. Connection issues or github api is unavailable? Status code received: ' + err.code), null);
            } // check if result is
            if (result.type && result.type === 'file') {
                if (result.encoding && result.encoding === 'base64') {
                    const configurationContents = Buffer.from(result.content, 'base64').toString('utf-8');
                    try {
                        const configFile = YAML.parse(configurationContents);
                        logger.debug(configFile);
                        return cb(null, configFile);
                    } catch(error) {
                        logger.error(error);
                        return cb(error, null);
                    }
                } else { // unsupported encoding type
                    logger.error('File encoding of .github-bot.yml returned by Github API is unsupported. Expected base64, received: ' + result.encoding);
                    return cb(new Error('File encoding of .github-bot.yml returned by Github API is unsupported. Expected base64, received: ' + result.encoding), null);
                }
            }// unsupported file type
            logger.error('File type of .github-bot.yml returned by Github API is unsupported. Expected file, received: ' + result.type);
            return cb(new Error('File type of .github-bot.yml returned by Github API is unsupported. Expected file, received: ' + result.encoding), null);

        });
    }

    function checkRepositoryIssues(repoConfig) {
        authenticate();
        if (!repoConfig.labels) {
            logger.error('No labels have been provided. Please provide labels for which to check');
            return;
        }

        repoConfig.labels.forEach(function(label) {
            github.issues.getForRepo({
                user: GITHUB_USER,
                repo: GITHUB_REPO,
                labels: label
            }, function(err, issues) {
                if (err) {
                    logger.error(err);
                } else if (issues) {
                    issues.forEach(function(issue) {
                        checkIssue(issue, repoConfig);
                    });
                }
            });
        });
    }

    function checkIssue(issue, repoConfig) {
        if (isExcluded(issue, repoConfig.exclude || [])) {
            return;
        }

        const updatedAt = new Date(issue.updated_at);
        const updatedDaysAgo = numDaysBetween(new Date(), updatedAt);
        if (updatedDaysAgo >= (repoConfig.days || MAX_DAYS_NO_UPDATE)) {
            if (repoConfig.action) {
                if (repoConfig.action.comment) {
                    commentOnTicket(issue.number, repoConfig.comment, {
                        days: Math.floor(updatedDaysAgo),
                        number: issue.number,
                        author: issue.user.login
                    }, logError);
                }
                if (repoConfig.action.close) {
                    closeGithubTicket(issue.number, logError);
                }
                if (repoConfig.action.assign_label || repoConfig.action.remove_label) {
                    logger.info('adjusting labels');
                    addLabels(issue.number, repoConfig.action.assign_label || [], function(err) {
                        if (err) {
                            logError(err);
                        } else {
                            removeLabels(issue.number, repoConfig.action.remove_label || [], logError);
                        }
                    });
                }
            }
        }
    }

    function isExcluded(issue, excludedLabels) {
        if (issue && issue.labels && excludedLabels) {
            let exclude = false;

            Object.keys(issue.labels).forEach(function(label) {
                if (excludedLabels.indexOf(issue.labels[label].name) >= 0) {
                    exclude = true;
                }
            });
            return exclude;
        }
        return false;
    }

    function closeGithubTicket(number, cb) {
        logger.info('Closing ticket #' + number);
        if (!PRODUCTION) {
            return cb(null, null);
        }
        authenticate();
        return github.issues.edit({
            user: GITHUB_USER,
            repo: GITHUB_REPO,
            number: number,
            state: 'closed',
            milestone: null,
            assignees: []
        }, cb);
    }

    function commentOnTicket(number, comment, forInString, cb) {
        if (!comment) {
            return cb(new Error('Comment is not provided'));
        }
        let replyBody = '';
        if (Array.isArray(comment)) {
            comment.forEach(function(line) {
                replyBody = replyBody + '\n\n' + line;
            });
        } else {
            replyBody = comment;
        }
        replyBody = replyBody.replace('{days}', forInString.days).replace('{author}', forInString.author);

        logger.info('Commenting on ticket #' + number);

        if (!PRODUCTION) {
            return cb(null, null);
        }
        authenticate();
        return github.issues.createComment({
            user: GITHUB_USER,
            repo: GITHUB_REPO,
            number: number,
            body: replyBody
        }, cb);
    }

    function addLabels(number, labels, cb) {
        if (!labels) {
            return cb(null, null);
        }

        logger.info('Adding labels to ticket #' + number + ':\n- ' + labels);
        if (!PRODUCTION) {
            return cb(null, null);
        }

        authenticate();
        return github.issues.addLabels({
            user: GITHUB_USER,
            repo: GITHUB_REPO,
            number: number,
            body: labels
        }, cb);
    }

    function removeLabels(number, labels, cb) {
        logger.info('Removing labels from ticket #' + number + ':\n- ' + labels);

        if (!PRODUCTION) {
            return cb(null, null);
        }
        return labels.forEach(function(label) {
            authenticate();
            github.issues.removeLabel({
                user: GITHUB_USER,
                repo: GITHUB_REPO,
                number: number,
                body: label
            }, logError);
        });
    }

    function authenticate() {
        if (GITHUB_TOKEN) {
            github.authenticate({
                type: 'oauth',
                token: GITHUB_TOKEN
            });
        }
    }

    const numDaysBetween = function(d1, d2) {
        const diff = d1.getTime() - d2.getTime();
        return diff / (1000 * 60 * 60 * 24);
    };

    const logError = function(err) {
        if (err) {
            logger.error(err);
        }
    };

    return {
        checkRepositoryIssues: checkRepositoryIssues,
        fetchRepoConfigurationFile: fetchRepoConfigurationFile
    };
};
