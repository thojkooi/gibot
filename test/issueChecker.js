'use strict';

const IssueChecker = require('./../lib/issuesChecker');
const assert = require('assert');

const MAX_DAYS_NO_UPDATE = 14;
const GITHUB_USER = 'thojkooi';
const GITHUB_REPO = 'gibot';
const GITHUB_TOKEN = 'hello-world';
const CLOSE_REASON_COMMENT = 'hello, I\'m a comment';

// set up our issue checking bot
const issueChecker = new IssueChecker({
    githubApi: { // mock githubApi
        issues: {
            getForRepo: function(config, cb) {
                cb(null, [{
                    'id': 123,
                    'number': 4281,
                    'state': 'open',
                    'locked': false,
                    'comments': 0,
                    'created_at': '2016-08-20T02:01:55Z',
                    'updated_at': '2016-08-20T02:01:55Z',
                    'closed_at': null,
                }]);
            },
            createComment: function(config, cb) {
                cb(null, null);
            },
            edit: function() {
            }
        },
        authenticate: function() {}
    },
    maxDaysNoUpdate: MAX_DAYS_NO_UPDATE,
    githubUser: GITHUB_USER,
    githubRepo: GITHUB_REPO,
    githubToken: GITHUB_TOKEN,
    production: false,
    reasonComment: CLOSE_REASON_COMMENT,
    closeOnTimeOut: true
});

describe('issueChecker', function() {
    describe('#init', function() {
        assert.ok(issueChecker);
    });
});
