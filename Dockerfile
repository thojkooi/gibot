FROM node:8.5.0-alpine

ENV MAX_DAYS_NO_UPDATE=14 \
    USER_AGENT='Gibot Github bot' \
    CLOSING_LABELS='need more info,invalid'

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --production

COPY . .
CMD ["node", "index.js"]
